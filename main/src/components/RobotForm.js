import React, { Component } from 'react'
import RobotStore from '../stores/RobotStore'
import Robot from './Robot'

class RobotForm extends Component{
    constructor(props){
        super(props);
        this.state={
            name:"",
            type:"",
            mass:""
        };
    }
    
    handleChangeName=(event)=>{
      this.setState({
          name:event.target.value
      })  
    };
     handleChangeType=(event)=>{
      this.setState({
          type:event.target.value
      })  
    };
     handleChangeMass=(event)=>{
      this.setState({
          mass:event.target.value
      })  
    };
    
  
    
    Add=()=>this.props.onAdd({
            name:this.state.name,
            type:this.state.type,
            mass:this.state.mass
    })
    
    render(){
        return(
            <div>
                <input type="text" id="name" name="name" onChange={this.handleChangeName} />
                 <input type="text" id="type" name="type" onChange={this.handleChangeType} />
                  <input type="text" id="mass" name="mass" onChange={this.handleChangeMass} />
                  <button  type="button" value="add" onClick={this.Add} />
            </div>
            );
    }
}


export default RobotForm;